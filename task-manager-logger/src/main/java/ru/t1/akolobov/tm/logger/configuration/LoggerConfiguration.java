package ru.t1.akolobov.tm.logger.configuration;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoDatabase;
import lombok.Setter;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.broker.BrokerService;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import ru.t1.akolobov.tm.logger.api.IPropertyService;

import javax.jms.ConnectionFactory;

@Setter
@Configuration
@ComponentScan("ru.t1.akolobov.tm.logger")
public class LoggerConfiguration {

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @Bean
    @NotNull
    public ConnectionFactory factory() {
        @NotNull final String url = propertyService.getConsumerUrl();
        @NotNull final ActiveMQConnectionFactory factory = new ActiveMQConnectionFactory(url);
        return factory;
    }

    @Bean
    @NotNull
    public MongoDatabase database() {
        @NotNull final MongoClient mongoClient = new MongoClient(
                propertyService.getDatabaseHost(),
                Integer.parseInt(propertyService.getDatabasePort())
        );
        return mongoClient.getDatabase(propertyService.getDatabaseName());
    }

    @Bean
    @NotNull
    public BrokerService brokerService() throws Exception {
        @NotNull final BrokerService brokerService = new BrokerService();
        if (Boolean.parseBoolean(propertyService.getServerEnabled())) {
            @NotNull final String url = propertyService.getConsumerUrl();
            brokerService.addConnector(url);
            brokerService.start();
        }
        return brokerService;
    }

}
