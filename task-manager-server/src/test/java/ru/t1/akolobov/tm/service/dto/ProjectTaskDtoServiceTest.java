package ru.t1.akolobov.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.t1.akolobov.tm.api.repository.dto.IProjectDtoRepository;
import ru.t1.akolobov.tm.api.repository.dto.ITaskDtoRepository;
import ru.t1.akolobov.tm.api.repository.dto.IUserDtoRepository;
import ru.t1.akolobov.tm.api.service.IPropertyService;
import ru.t1.akolobov.tm.api.service.dto.IProjectTaskDtoService;
import ru.t1.akolobov.tm.configuration.ServerConfiguration;
import ru.t1.akolobov.tm.dto.model.ProjectDto;
import ru.t1.akolobov.tm.dto.model.TaskDto;
import ru.t1.akolobov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.akolobov.tm.exception.entity.TaskNotFoundException;
import ru.t1.akolobov.tm.exception.field.ProjectIdEmptyException;
import ru.t1.akolobov.tm.exception.field.TaskIdEmptyException;
import ru.t1.akolobov.tm.exception.field.UserIdEmptyException;
import ru.t1.akolobov.tm.marker.UnitCategory;

import javax.persistence.EntityManager;
import java.util.List;

import static ru.t1.akolobov.tm.data.dto.TestProjectDto.createProjectList;
import static ru.t1.akolobov.tm.data.dto.TestTaskDto.createTaskList;
import static ru.t1.akolobov.tm.data.dto.TestUserDto.*;

@Category(UnitCategory.class)
public class ProjectTaskDtoServiceTest {

    @NotNull
    private final static ApplicationContext context =
            new AnnotationConfigApplicationContext(ServerConfiguration.class);

    @NotNull
    @Autowired
    private static IPropertyService propertyService;

    @NotNull
    @Autowired
    private static IUserDtoRepository userRepository;

    @NotNull
    private final static EntityManager repositoryEntityManager = userRepository.getEntityManager();

    @NotNull
    @Autowired
    private ITaskDtoRepository taskRepository;

    private EntityManager taskEntityManager = taskRepository.getEntityManager();

    @NotNull
    @Autowired
    private IProjectDtoRepository projectRepository;

    private EntityManager projectEntityManager = projectRepository.getEntityManager();

    @NotNull
    @Autowired
    private IProjectTaskDtoService service;

    @BeforeClass
    public static void addUsers() {
        repositoryEntityManager.getTransaction().begin();
        userRepository.add(USER1);
        userRepository.add(USER2);
        repositoryEntityManager.getTransaction().commit();
    }

    @AfterClass
    public static void clearUsers() {
        repositoryEntityManager.getTransaction().begin();
        userRepository.remove(USER1);
        userRepository.remove(USER2);
        repositoryEntityManager.getTransaction().commit();
        repositoryEntityManager.close();
    }

    @Before
    public void initRepository() {
        taskEntityManager.getTransaction().begin();
        createTaskList(USER1_ID).forEach(taskRepository::add);
        taskEntityManager.getTransaction().commit();
        projectEntityManager.getTransaction().begin();
        createProjectList(USER1_ID).forEach(projectRepository::add);
        projectEntityManager.getTransaction().commit();
    }

    @After
    public void clearRepository() {
        taskEntityManager.getTransaction().begin();
        projectEntityManager.getTransaction().begin();
        taskRepository.clear(USER1_ID);
        projectRepository.clear(USER1_ID);
        taskRepository.clear(USER2_ID);
        projectRepository.clear(USER2_ID);
        taskEntityManager.getTransaction().commit();
        projectEntityManager.getTransaction().commit();
    }

    @Test
    public void bindTaskToProject() {
        @NotNull final ProjectDto project = projectRepository.findAll(USER1_ID).get(0);
        @NotNull final TaskDto task = taskRepository.findAll(USER1_ID).get(0);
        service.bindTaskToProject(USER1_ID, project.getId(), task.getId());
        taskEntityManager.refresh(task);
        final TaskDto bindTask = taskRepository.findOneById(USER1_ID, task.getId());
        Assert.assertNotNull(bindTask);
        Assert.assertEquals(project.getId(), bindTask.getProjectId());

        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> service.bindTaskToProject(USER_EMPTY_ID, project.getId(), task.getId())
        );
        Assert.assertThrows(
                ProjectIdEmptyException.class,
                () -> service.bindTaskToProject(USER1_ID, USER_EMPTY_ID, task.getId())
        );
        Assert.assertThrows(
                TaskIdEmptyException.class,
                () -> service.bindTaskToProject(USER1_ID, project.getId(), USER_EMPTY_ID)
        );
        Assert.assertThrows(
                TaskNotFoundException.class,
                () -> service.bindTaskToProject(USER1_ID, project.getId(), project.getId())
        );
        Assert.assertThrows(
                ProjectNotFoundException.class,
                () -> service.bindTaskToProject(USER1_ID, task.getId(), project.getId())
        );
    }

    @Test
    public void removeProjectById() {
        @NotNull final ProjectDto project = projectRepository.findAll(USER1_ID).get(0);
        @NotNull final List<TaskDto> taskList = taskRepository.findAll(USER1_ID);
        service.bindTaskToProject(USER1_ID, project.getId(), taskList.get(0).getId());
        service.bindTaskToProject(USER1_ID, project.getId(), taskList.get(1).getId());

        service.removeProjectById(USER1_ID, project.getId());
        Assert.assertEquals(taskList.size() - 2, taskRepository.findAll(USER1_ID).size());
        Assert.assertFalse(taskRepository.findAll(USER1_ID).contains(taskList.get(0)));
        Assert.assertFalse(taskRepository.findAll(USER1_ID).contains(taskList.get(1)));

        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> service.removeProjectById(USER_EMPTY_ID, project.getId())
        );
        Assert.assertThrows(
                ProjectIdEmptyException.class,
                () -> service.removeProjectById(USER1_ID, USER_EMPTY_ID)
        );
    }

    @Test
    public void unbindTaskFromProject() {
        @NotNull final ProjectDto project = projectRepository.findAll(USER1_ID).get(0);
        @NotNull final TaskDto task = taskRepository.findAll(USER1_ID).get(0);
        service.bindTaskToProject(USER1_ID, project.getId(), task.getId());
        taskEntityManager.refresh(task);
        TaskDto bindTask = taskRepository.findOneById(USER1_ID, task.getId());
        Assert.assertNotNull(bindTask);
        Assert.assertEquals(project.getId(), bindTask.getProjectId());

        service.unbindTaskFromProject(USER1_ID, task.getId());
        taskEntityManager.refresh(bindTask);
        bindTask = taskRepository.findOneById(USER1_ID, task.getId());
        Assert.assertNotNull(bindTask);
        Assert.assertNotEquals(project.getId(), bindTask.getProjectId());
        Assert.assertNull(bindTask.getProjectId());

        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> service.unbindTaskFromProject(USER_EMPTY_ID, task.getId())
        );
        Assert.assertThrows(
                TaskIdEmptyException.class,
                () -> service.unbindTaskFromProject(USER1_ID, USER_EMPTY_ID)
        );
        Assert.assertThrows(
                TaskNotFoundException.class,
                () -> service.unbindTaskFromProject(USER1_ID, project.getId())
        );
    }

}
